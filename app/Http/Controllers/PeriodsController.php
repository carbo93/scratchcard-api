<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use App\Repositories\PeriodsRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class PeriodsController extends Controller
{
    /**
     */
    protected $periods;
    
    /**
     * TestController constructor.
     * @param $posts
     */
    public function __construct(PeriodsRepository $periods)
    {
        $this->periods = $periods;
        //exit;
    }
    public function odds(Request $request)
    {
        //取得刮刮樂彩金類別
        $all = $request->all();
        
        if(isset($all['periods_id']) && intval($all['periods_id']) > 0 ){
            $periods_id = intval($all['periods_id']);
            $res = $this->periods->odds($periods_id);
            if($res){
                return Response::json(array(
                    'status'      =>  true,
                    'data'   =>  $res
                ), 200);
            }
        }
        return Response::json(array(
            'status'      =>  false,
            'msg' =>'odds error',
        ), 500);
    }
    public function class(Request $request)
    {
        //取得刮刮樂總類
        $all = $request->all();
        $res = $this->periods->class();
        if($res){
            
            return Response::json(array(
                'status'      =>  true,
                'data'   =>  $res
            ), 200);
        }
        return Response::json(array(
            'status'      =>  false,
            'msg' =>'class error',
        ), 500);
    }
    public function list(Request $request)
    {
        //取得刮刮樂期數
        $all = $request->all();
        if(isset($all['class_id']) && intval($all['class_id']) > 0 ){
            $class_id = intval($all['class_id']);
            $res = $this->periods->list($class_id);
            if($res){
                foreach($res as $key=>$value){
                    $res2 = $this->periods->book($value['periods_id']);
                    if($res2){
                        $res[$key]['book']=$res2;
                    }
                }
                return Response::json(array(
                    'status'      =>  true,
                    'data'   =>  $res
                ), 200);
            }
        }
        return Response::json(array(
            'status'      =>  false,
            'msg' =>'list error',
        ), 500);
    }
    public function remaining(Request $request){
        //剩餘獎品
        $all = $request->all();
        if(isset($all['periods_id']) && intval($all['periods_id']) > 0 ){
            $periods_id = intval($all['periods_id']);
            $res = $this->periods->remaining($periods_id);
            if($res){
                return Response::json(array(
                    'status'      =>  true,
                    'data'   =>  $res
                ), 200);
            }
        }
        return Response::json(array(
            'status'      =>  false,
            'msg' =>'remaining error',
        ), 500);
    }
    public function piece(Request $request){
        $all = $request->all();
        if(isset($all['bookserial']) && intval($all['bookserial']) > 0 ){
            $bookserial = intval($all['bookserial']);
            $res = $this->periods->piece($bookserial);
            if($res){
                return Response::json(array(
                    'status'      =>  true,
                    'data'   =>  $res
                ), 200);
            }
        }
        return Response::json(array(
            'status'      =>  false,
            'msg' =>'piece error',
        ), 500);
    }
}