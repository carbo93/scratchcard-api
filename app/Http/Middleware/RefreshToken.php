<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Response;
use Closure;
use App\Repositories\GameLoginRepository;
use App\Repositories\WalletRepository;

class RefreshToken 
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    private function getBearerToken($request) {
        $headers = $request->header('Authorization');
        // HEADER: Get the access token from the header
        if (!empty($headers)) {
            if (preg_match('/Bearer\s(\S+)/', $headers, $matches)) {
                return $matches[1];
            }if (preg_match('/bearer\s(\S+)/', $headers, $matches)) {
                return $matches[1];
            }
        }
        return null;
    }
    private function deBearerToken($token) {
        $de = _encrypt($token,"D",env("JWT_SECRET"));
        if (!empty($de)) {
            return explode("|", $de);
        }
        return null;
    }
    public function handle($request, Closure $next)
    {
        $authorization = "";
        if(env("APP_DEBUG")){
            $login = new GameLoginRepository;
            $res = $login->FakeLogin("xgame_test", "123456");
            $guid = guid();
            $input = array();
            $input['token'] = $res['token'];
            $input['game_id'] = env("GAME_ID");
            $input['conn_id'] = $guid;
            $res = $login->AuthAck($input);
            if($res){
                $authorization = _encrypt($guid."|".date("Y-m-d")."|".$input['token']."|".$res['game_token']."|".$res['user_id'],"E",env("JWT_SECRET"));
            }
        }else{
            $authorization = $this->getBearerToken($request);
        }
        if($authorization != null){
            $get = $this->deBearerToken($authorization);
            if($get != null){
                if($get[1] == date("Y-m-d")){
                    $merge = array();
                    $merge['conn_id'] = $get[0];
                    $merge['token'] = $get[2];
                    $merge['game_token'] = $get[3];
                    $merge['game_id'] = env("GAME_ID");
                    $merge['user_id'] = $get[4];
                    if(env("APP_DEBUG")){
                        $merge['coin_type'] = 1;
                        $merge['coin_amount'] = 100;
                        $wallet = new WalletRepository;
                        $res = $wallet->walletExchangeAck($merge);
                        unset($merge['coin_type']);
                        unset($merge['coin_amount']);
                        
                    }
                    $request->merge($merge);
                    return $next($request);
                }
                
            }
        }
        return Response::json(array(
            'status'      =>  false,
            'RefreshToken Error',
        ), 500);
    }
}
