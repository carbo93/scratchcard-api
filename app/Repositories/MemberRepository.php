<?php

namespace App\Repositories;

use App\Models\Scratch;
use App\Models\MemberScratch;
use Illuminate\Support\Facades\DB;
class MemberRepository  {
    /*
     *
     primary key: pk_<table>
     foreign_key: fk_<parent_table>_<chid_table>_<sequence>
     unique index: uk_<table>_<column>
     index: idx_<table>_<column>
     */
    /**
     * TestController constructor.
     * @param $posts
     */
    public function __construct()
    {
    }
    public function products($user_id){
        if($user_id !=""){
            $db = new MemberScratch;
            $res = $db->where("user_id","=",$user_id)->where("status","=",1)->where("is_used","=",0)->get();
            if($res){
                $res = $res->toArray();
                if(!empty($res)){
                    $ids = array();
                    foreach($res as $value){
                        $ids[] = $value['scratchs_id'];
                    }
                    $scratch = new ScratchRepository;
                    return $scratch->productsByIds($ids);
                }
            }
        }
        return false;
    }
    public function productsResult($user_id,$ids = array()){
        if($user_id !=""){
            $memberScratch = new MemberScratch;
            $res = $memberScratch->where("user_id","=",$user_id)->where("status","=",1)->where("is_used","=",0)->get();
            if($res){
                $res = $res->toArray();
                if(!empty($res)){
                    $out = array();
                    $out['amoutsn']= array();
                    $out['amouttag']= array();
                    $out['amout']=0;
                    $where = array();
                    if(!empty($ids)){
                        foreach($res as $value){
                            foreach($ids as $value2){
                                if($value['scratchs_id'] == $value2){
                                    $where[] = $value['scratchs_id'];
                                }
                            }
                        }
                    }else{
                        foreach($res as $value){
                            $where[] = $value['scratchs_id'];
                        }
                    }
                    if(!empty($where)){
                        $scratch = new ScratchRepository;
                        $res = $scratch->productsByIds($where);
                        foreach($res as $value){
                            $memberScratch->where("scratchs_id","=",$value['id'])->update(array("is_used"=>1));
                            if(isset($out['amouttag'][$value['amount']])){
                                $out['amouttag'][$value['amount']] = $out['amouttag'][$value['amount']]+1;
                            }else{
                                $out['amouttag'][$value['amount']] = 1;
                            }
                            $out['amoutsn'][$value['amount']][]=$value['sn'];
                            $out['amout'] = $out['amout']+$value['amount'];
                        }
                        return $out;
                    }
                }
            }
        }
        return false;
    }
}