<?php

namespace App\Repositories;

use App\Models\Period;
use App\Models\PeriodsClass;
use App\Models\PeriodsBook;
use App\Models\Scratch;
use App\Models\MemberScratch;
use Illuminate\Support\Facades\DB;
use App\Repositories\OrderRepository;
class ScratchRepository  {
    /*
     *
     primary key: pk_<table>
     foreign_key: fk_<parent_table>_<chid_table>_<sequence>
     unique index: uk_<table>_<column>
     index: idx_<table>_<column>
     */
    protected $order;
    /**
     * TestController constructor.
     * @param $posts
     */
    public function __construct()
    {
        //exit;
    }
    public function productsByIds($ids = array()){
        if(!empty($ids)){
            $db = new Scratch;
            $res = $db->whereIN('id',$ids)->with('period')->get();
            if($res){
                $res = $res->toArray();
                return $res;
            }
        }
        return false;
    }
    public function recovery($id = 0){
        if($id > 0){
                $db = new Scratch;
                $res = $db->where('id','=',$id)->update(array('status'=>2));
                if($res){
                    $db = new MemberScratch;
                    $res = $db->where('id','=',$id)->update(array('status'=>2));
                    if($res){
                        return true;
                    }
                }
        }
        return false;
    }
    public function checkoutByU168($items = array(),$all){
        $this->order = new OrderRepository;
        if(!empty($items)){
            $price = 0;
            foreach ($items as $item){
                $price = $price+$item['period']['price'];
            }
            $input = array();
            /*
             * token *
             game_id *
             game_token *
             game_type *
             game_round *
             currency *
             amount *
             status 1
             */
            $input['token'] = $all['token'];
            $input['game_id'] = env("GAME_ID");
            $input['game_token'] = $all['game_token'];
            $input['game_type'] = "scratchcard";
            $input['game_round'] = date("Y-m-d")."-000-00-000";
            $input['currency_id'] = 99;
            $input['amount'] = $price;
            $input['status'] = 1;
            $input['created_at'] = date("Y-m-d")."T".date("H:i:s")."Z";
            $createOrder =$this->order->createOrder($input);
            if($createOrder){
                $order = $createOrder['order'];
                foreach ($items as $id=>$item){
                    $input = array();
                    $input['order_uuid'] = $order['uuid'];
                    $input['token'] = $all['token'];
                    $input['game_id'] = env("GAME_ID");
                    $input['game_token'] = $all['game_token'];
                    $input['play_code'] = $item['sn']; //??
                    $input['summary'] = $item['period']['periods_class_id']."-".$item['periods_id']; //??
                    $input['currency_id'] = 99;
                    $input['amount'] = $item['period']['price'];
                    $input['status'] = 1;
                    $input['created_at'] = date("Y-m-d")."T".date("H:i:s")."Z";
                    $createOrderItem =$this->order->createOrderItem($input);
                    if(isset($createOrderItem)){
                        $orderItem = $createOrderItem['order_item'];
                        $input = array(
                            "token" => $all["token"],
                            "game_id" => env("GAME_ID"),
                            "game_token" => $all['game_token'],
                            "rate" => 1,
                            "win" => $item['amount'],
                            "remark" => "",
                            "payout_at" => date("Y-m-d")."T".date("H:i:s")."Z",
                            "result" => json_encode($item),
                        );
                        $input['order_uuid'] = $order['uuid'];
                        $input['item_uuid'] = $orderItem['uuid'];
                        $payoutOrderItem =$this->order->payoutOrderItem($input);
                        if($payoutOrderItem){
                            
                        }else{
                            return false;
                        }
                    }
                }
                $input = array();
                $input['order_uuid'] = $order['uuid'];
                $input['token'] = $all['token'];
                $input['game_id'] = env("GAME_ID");
                $input['game_token'] = $all['game_token'];
                $input['payout_at'] = date("Y-m-d")."T".date("H:i:s")."Z";
                $payoutOrder =$this->order->payoutOrder($input);
                if($payoutOrder){
                    return true;
                }
            }
        }
        return false;
    }
    public function checkout($ids = array(),$user_id =""){
        $out = array();
        $out[0] = array();
        $out[1] = array();
        if(!empty($ids) && $user_id!=""){
            foreach ($ids as $id){
                try {
                    DB::connection()->getPdo()->beginTransaction();
                    $db = new Scratch;
                    //->where('user_id','=',NULL)
                    $data = $db->where('user_id','=',NULL)->where('id','=',$id)->where('status','=',0)->lockForUpdate()->get();
                    if(!empty($data->toArray())){
                        $res = $db->where('id','=',$id)->update(array('user_id'=>$user_id,'status'=>1));
                        if($res){
                            $db = new MemberScratch;
                            $db->user_id = $user_id;
                            $db->scratchs_id = $id;
                            $db->is_used = 0;
                            $db->status = 1;
                            $db->save();
                            $out[1][$id]=$id;
                        }else{
                            $out[0][$id]=$id;
                        }
                    }else{
                        $out[0][$id]=$id;
                    }
                    DB::connection()->getPdo()->commit();
                } catch (\Illuminate\Database\Eloquent\ModelNotFoundException $e){
                    //echo $e->getMessage();
                    $out[0][$id]=$id;
                    DB::connection()->getPdo()->rollBack();
                } catch (\Illuminate\Database\QueryException $e) {
                    //echo $e->getMessage();
                    $out[0][$id]=$id;
                    DB::connection()->getPdo()->rollBack();
                } catch (\PDOException $e) {
                    //echo $e->getMessage();
                    $out[0][$id]=$id;
                    DB::connection()->getPdo()->rollBack();
                }catch (\Throwable $e) {
                    //echo $e->getMessage();
                    $out[0][$id]=$id;
                    DB::connection()->getPdo()->rollBack();
                }
            }
            return $out;
        }
        return false;
    }
}