<?php

namespace App\Repositories;

use App\Models\Period;
use App\Models\PeriodsClass;
use App\Models\PeriodsBook;
use App\Models\Scratch;
use App\Models\Odd;
use Illuminate\Support\Facades\DB;
class PeriodsRepository  {
    /*
     *
     primary key: pk_<table>
     foreign_key: fk_<parent_table>_<chid_table>_<sequence>
     unique index: uk_<table>_<column>
     index: idx_<table>_<column>
     */
    public function __construct(){
        
    }
    public function class(){
        $db = new PeriodsClass;
        $res = $db->query()->select('id as periods_class_id','name','status')->with('period')->get();
        if($res){
            return $res->toArray();
        }
        return false;
        
    }
    public function remaining($periods_id){
        $total = 0;
        if($periods_id>0){
            $db = new Scratch;
            $res = $db->where('periods_id',$periods_id)->where('amount',"!=",0)->get();
            if($res){
                $res = $res->toArray();
                foreach($res as $value){
                    $total = $total + $value['amount'];
                }
            }
        }
        return $total;
    }
    public function list($class_id){
        $db = new Period;
        $res = $db->query()->select('id as periods_id','periods_class_id','sn','price')->where(array('status'=>1))->where(array('periods_class_id'=>$class_id))->whereDate('issued_at', '<=', date("Y-m-d"))->get();
        if($res){
            return $res->toArray();
        }
        return false;
        
    }
    public function odds($periods_id){
        $db = new Odd;
        $res = $db->query()->where(array('status'=>1))->where(array('periods_id'=>$periods_id))->get();
        if($res){
            $out = array();
            foreach($res->toArray() as $key=>$value){
                $out[$key]=$value['amout'];
            }
            rsort($out);
            return $out;
        }
        return false;
        
    }
    public function book($periods_id){
        $db = new PeriodsBook;
        $res = $db->query()->where(array('status'=>1))->where(array('periods_id'=>$periods_id))->get();
        if($res){
            $out = array();
            foreach($res->toArray() as $key=>$value){
                $db = new Scratch;
                $count = $db->query()->where(array('status'=>0))->where('user_id','=',NULL)->where(array('bookserial'=>$value['bookserial']))->count();
                $out[$key]=$value;
                $out[$key]['count']=$count;
            }
            return $out;
        }
        return false;
        
    }
    public function piece($bookserial){
        $db = new Scratch;
        $res = $db->query()->whereIn('status',[0,1])->where(array('bookserial'=>$bookserial))->get();
        if($res){
            return $res->toArray();
        }
        return false;
        
    }
}