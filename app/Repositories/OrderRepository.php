<?php 
namespace App\Repositories;

class OrderRepository
{
    protected $ulg_api;
    protected $order_id;

    public function __construct()
    {
        //$url, $params = false, $type = 'GET', $https = 0
        $this->ulg_api = env('ULG168_API_URL');
    }

    public function createOrder($data)
    {

        /*
token *


game_id *


game_token *


game_type *


game_round *


currency *

amount *



status 1


created_at 2018-10-11T10:08:00Z	
         * */
        
        
        $url = $this->ulg_api . "/v1/apis/ulg168/order";

        $res = curl($url,$data,"POST",1);
        if ($res['httpCode'] == 200) {
            return json_decode($res['response'], true);
        }
        return false;

    }

    public function createOrderItem($data)
    {

        $url = $this->ulg_api . "/v1/apis/ulg168/order/" . $data["order_uuid"] . "/item";

        $res = curl($url,$data,"POST",1);
        if ($res['httpCode'] == 200) {
            return json_decode($res['response'], true);
        }
        return false;

    }

    public function payoutOrder($data)
    {

        $url = $this->ulg_api . "/v1/apis/ulg168/order/" . $data["order_uuid"] . "/payout";

        $res = curl($url,$data,"PUT",1);
        if ($res['httpCode'] == 200) {
            return json_decode($res['response'], true);
        }
        return false;

    }

    public function payoutOrderItem($data)
    {
        $url = $this->ulg_api . "/v1/apis/ulg168/order/" . $data["order_uuid"] . "/item/" . $data["item_uuid"] . "/payout";
        $res = curl($url,$data,"PUT",1);
        if ($res['httpCode'] == 200) {
            return json_decode($res['response'], true);
        }
        return false;

    }

    private function json_decode($msg_array)
    {

        return json_decode($msg_array, true);

    }


} 
