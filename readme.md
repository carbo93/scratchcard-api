# template

### local domain
```
http://local-api.scratchcard.com
```

## Project setup
```
composer install
```

##DataBase
```
import scratchcard_2018-11-21.sql
``` 

##Setting Environment
```
copy .env.example to .env
``` 

